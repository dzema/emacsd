(global-set-key (kbd "C-c C-r") 'revert-buffer)

(global-set-key (kbd "C-c C-t") 'run-spec)
(global-set-key (kbd "C-c C-T") 'run-specs)


(global-set-key (kbd "C-x b") 'ido-switch-buffer)

(global-set-key (kbd "M-RET") 'ns-toggle-fullscreen)

(global-set-key (kbd "C-c h") 'helm-resume)

;; Activates 'browse-kill-ring if trying to M-y without
;; yanking (C-y) something first.
;; require browse-kill-ring package
(when (require 'browse-kill-ring nil 'noerror)
  (browse-kill-ring-default-keybindings))

(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)

(global-set-key (kbd "C-c C-i b") 'ispell-buffer)
(global-set-key (kbd "C-c C-i w") 'ispell-word)

;; Smex is ido-style for M-x
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)

;; expand-region

(global-set-key (kbd "C-@") 'er/expand-region)
