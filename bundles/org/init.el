(require 'org)
;; use org-mode for .org files
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(add-to-list 'auto-mode-alist '("\\.org.erb$" . org-mode))
(add-to-list 'auto-mode-alist '("\\.notes?$" . org-mode))

;; load bundle snippets
;;(yas/load-directory "~/emacsd/bundles/org/snippets")

(setq org-log-done 'time)
(setq org-log-done 'note)

(setq dzema-org-base
      (if (file-exists-p "~/Documents/icloud/notes.org")
          "~/Documents/icloud/"
        "~/Documents/"))

(setq org-default-notes-file (concat dzema-org-base "notes.org"))

(setq org-agenda-files
      (append
       (directory-files-recursively dzema-org-base "org$")
       (directory-files-recursively "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/Documents/" "org$")))

(setq org-refile-use-outline-path 'file)

(setq org-refile-targets
      '((nil :maxlevel . 3)
        (org-agenda-files :maxlevel . 3)))


(add-hook 'org-agenda-mode-hook
          (lambda ()
            (toggle-truncate-lines 0)))

(add-hook 'org-mode-hook
          (lambda ()
            (toggle-truncate-lines 0)))

