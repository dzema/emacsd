;; load bundle snippets
;; (yas/load-directory "~/emacsd/bundles/xml/snippets")

(add-to-list 'auto-mode-alist '("\\.xml$" . nxml-mode))