(require 'markdown-mode)

;; (yas/load-directory "~/emacsd/bundles/markdown/snippets")

(autoload 'markdown-mode "markdown-mode"
  "Major mode for editing Markdown files" t)

(setq mardown-command "gfm")
(add-to-list 'auto-mode-alist '("\\.markdown$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
