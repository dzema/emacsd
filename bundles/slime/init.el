(add-to-list 'load-path "~/emacsd/bundles/slime/swank-clojure")
(add-to-list 'load-path "~/emacsd/bundles/slime/slime")

(require 'clojure-mode)
(require 'swank-clojure-autoload)

(swank-clojure-config
 (setq swank-clojure-jar-path "~/src/clojure/clojure.jar")
 (setq swank-clojure-extra-classpaths
       (list "~/src/clojure-contrib/clojure-contrib.jar")))

(eval-after-load "slime"
  '(progn (slime-setup '(slime-repl))))

(setq inferior-lisp-program "~java -jar /Users/dzema/src/clojure/clojure.jar")

(require 'slime)
;;(slime-setup)
