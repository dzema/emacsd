(load-library "haskell-site-file")

(require 'haskell-mode)
(add-to-list 'auto-mode-alist '("\\.hs$" . haskell-mode))

(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)

;;(yas/load-directory "~/emacsd/bundles/haskell/snippets")

(define-key haskell-mode-map "\C-ch" 'haskell-hoogle)
;(setq haskell-hoogle-command "hoogle")

(setq browse-url-browser-function 'browse-url-in-default-browser)
(defun browse-url-in-default-browser (url &optional new-window)
 "Open URL in default system browser on macOS."
 (interactive (browse-url-interactive-arg "URL: "))
 (unless
     (string= ""
              (shell-command-to-string
               (concat "open " url)))
   (message "Opening browser...")
   (start-process (concat "open" url) nil "open" url)))
