;;; Shell

(global-set-key (kbd "C-x m") 'term)
(global-set-key (kbd "C-x C-m") 'term)
(global-set-key (kbd "C-c m") 'term)