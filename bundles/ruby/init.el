(require 'ruby-mode)
(require 'inf-ruby)
(require 'ruby-electric)
(require 'yari)
;;(require 'autotest)
;;(require 'toggle)


;; load bundle snippets
;;(yas/load-directory "~/emacsd/bundles/ruby/snippets")

(add-to-list 'auto-mode-alist '("\\.rb$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.rake$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Gemfile$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Capfile$" . ruby-mode))

(define-key 'help-command "R" 'yari)
