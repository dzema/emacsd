

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>vendor/treetop.el at master from defunkt's emacs &mdash; GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="http://github.com/fluidicon.png" title="GitHub" />

    
      <link href="http://assets1.github.com/stylesheets/bundle.css?366981d3fa18abe465a1292caaaf4c5a5a8425e9" media="screen" rel="stylesheet" type="text/css" />
    

    
      
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
        <script src="http://assets0.github.com/javascripts/bundle.js?366981d3fa18abe465a1292caaaf4c5a5a8425e9" type="text/javascript"></script>
      
    
    
  
    
  

  <link href="http://github.com/feeds/defunkt/commits/emacs/master" rel="alternate" title="Recent Commits to emacs:master" type="application/atom+xml" />


    
  </head>

  

  <body>
    
      <div id="site_alert">
        <div class="site">
          <p class="corner">
We got nominated! Help us out and 
<a href="http://crunchies2008.techcrunch.com/votes/?nominee_id=8&category_id=2">vote for GitHub</a>
as Best Bootstrapped Startup of 2008. (You can vote once a day.)
<small><a href="#" class="hide_alert">[ hide ]</a></small>
          </p>
        </div>
      </div>
    

    <div id="main">
      <div id="header" class="">
        <div class="site">
          <div class="logo">
            <a href="/"><img src="/images/modules/header/logo.png" alt="git-hub" /></a>
          </div>
          
            <div class="actions">
              <a href="/">Home</a>
              <a href="/plans"><b><u>Pricing and Signup</u></b></a>
              <a href="/repositories">Repositories</a>
              <a href="/guides">Guides</a>
              <a href="/blog">Blog</a>
              <a href="/login">Login</a>
            </div>
          
        </div>
      </div>
      
      
        
  <div id="repo_menu">
    <div class="site">
      <ul>
        
          <li class="active"><a href="/defunkt/emacs/tree/master">Source</a></li>

          <li class=""><a href="/defunkt/emacs/commits/master">Commits</a></li>

          <li class=""><a href="/defunkt/emacs/graphs">Graphs</a></li>

          <li class=""><a href="/defunkt/emacs/wikis">Wiki (1)</a></li>

          <li class=""><a href="/defunkt/emacs/watchers">Watchers (32)</a></li>

          <li class=""><a href="/defunkt/emacs/network">Network (9)</a></li>

          
          
          

        
      </ul>
    </div>
  </div>

  <div id="repo_sub_menu">
    <div class="site">
      <div class="joiner"></div>
      

      
      
      

      
        <ul>
          <li>
            <a class="active" href="/defunkt/emacs/tree/45480d798311c870915ee9d1b3860cb33cda3f72">master</a>
          </li>
          <li>
            <a href="#">all branches</a>
            <ul>
              
                
                  <li><a href="/defunkt/emacs/tree/master">master</a></li>
                
              
            </ul>
          </li>
          <li>
            <a href="#">all tags</a>
            
          </li>
        </ul>

      
    </div>
  </div>

  <div class="site">
    





<div id="repos">
  



  <div class="repo public">
    <div class="title">
      <div class="path">
        <a href="/defunkt">defunkt</a> / <b><a href="/defunkt/emacs/tree">emacs</a></b>

        

          

          
            

            
              
              <a href="/signup"><img alt="fork" class="button" src="http://assets0.github.com/images/modules/repos/fork_button.png?366981d3fa18abe465a1292caaaf4c5a5a8425e9" /></a>
            
          

          <a href="/signup" class="toggle_watch"><img alt="watch" class="button" src="http://assets0.github.com/images/modules/repos/watch_button.png?366981d3fa18abe465a1292caaaf4c5a5a8425e9" /></a><a href="/signup" class="toggle_watch" style="display:none;"><img alt="watch" class="button" src="http://assets3.github.com/images/modules/repos/unwatch_button.png?366981d3fa18abe465a1292caaaf4c5a5a8425e9" /></a>

          
            <a href="#" id="download_button" rel="/defunkt/emacs/downloads/master"><img alt="download tarball" class="button" src="http://assets2.github.com/images/modules/repos/download_button.png?366981d3fa18abe465a1292caaaf4c5a5a8425e9" /></a>
          
        
      </div>

      <div class="security private_security" style="display:none">
        <a href="#private_repo" rel="facebox"><img src="/images/icons/private.png" alt="private" /></a>
      </div>

      <div id="private_repo" class="hidden">
        This repository is private.
        All pages are served over SSL and all pushing and pulling is done over SSH.
        No one may fork, clone, or view it unless they are added as a <a href="/defunkt/emacs/edit/collaborators">member</a>.

        <br/>
        <br/>
        Every repository with this icon (<img src="/images/icons/private.png" alt="private" />) is private.
      </div>

      <div class="security public_security" style="">
        <a href="#public_repo" rel="facebox"><img src="/images/icons/public.png" alt="public" /></a>
      </div>

      <div id="public_repo" class="hidden">
        This repository is public.
        Anyone may fork, clone, or view it.

        <br/>
        <br/>
        Every repository with this icon (<img src="/images/icons/public.png" alt="public" />) is public.
      </div>

      
    </div>
    <div class="meta">
      <table>
        
        
          <tr>
            <td class="label">Description:</td>
            <td>
              <span id="repository_description" rel="/defunkt/emacs/edit/update" class="">My Emacs config</span>
              
            </td>
          </tr>
        

        
          

          
            <tr>
              <td class="label">Clone&nbsp;URL:</td>
              
              <td>
                <a href="git://github.com/defunkt/emacs.git" class="git_url_facebox" rel="#git-clone">git://github.com/defunkt/emacs.git</a>
                <div id="git-clone" style="display:none;">
                  Give this clone URL to anyone.
                  <br/>
                  <code>git clone git://github.com/defunkt/emacs.git </code>
                </div>
              </td>
            </tr>
          
          
          

          

          
      </table>

          </div>
  </div>




</div>

<div id="commit">
  <div class="group">
      
  <div class="envelope commit">
    <div class="human">
      
        <div class="message"><pre><a href="/defunkt/emacs/commit/45480d798311c870915ee9d1b3860cb33cda3f72">guard aquamacs specific stuff</a> </pre></div>
      

      <div class="actor">
        <div class="gravatar">
          
          <img alt="" height="30" src="http://www.gravatar.com/avatar/b8dbb1987e8e5318584865f880036796?s=30&amp;d=http%3A%2F%2Fgithub.com%2Fimages%2Fgravatars%2Fgravatar-30.png" width="30" />
        </div>
        <div class="name"><a href="/defunkt">defunkt</a> <span>(author)</span></div>
          <div class="date">
            <abbr class="relatize" title="2008-11-27 18:18:12">Thu Nov 27 18:18:12 -0800 2008</abbr> 
          </div>
      </div>
  
      
  
    </div>
    <div class="machine">
      <span>c</span>ommit&nbsp;&nbsp;<a href="/defunkt/emacs/commit/45480d798311c870915ee9d1b3860cb33cda3f72" hotkey="c">45480d798311c870915ee9d1b3860cb33cda3f72</a><br />
      <span>t</span>ree&nbsp;&nbsp;&nbsp;&nbsp;<a href="/defunkt/emacs/tree/45480d798311c870915ee9d1b3860cb33cda3f72/vendor/treetop.el" hotkey="t">c4739f45778c97b83e9a04c0652e6df46c40a972</a><br />
  
      
        <span>p</span>arent&nbsp;
        
        <a href="/defunkt/emacs/tree/ee26c3c1b6533ac457353e54d55b0a00c4c4756d/vendor/treetop.el" hotkey="p">ee26c3c1b6533ac457353e54d55b0a00c4c4756d</a>
      
  
    </div>
  </div>

  </div>
</div>





  




  
    <div id="path">
      <b><a href="/defunkt/emacs/tree">emacs</a></b> / <a href="/defunkt/emacs/tree/master/vendor">vendor</a> / treetop.el
    </div>

    
      <div id="files">
  <div class="file">
    <div class="meta">
      <div class="info">
        <span>100644</span>
        <span>40 lines (33 sloc)</span>
        <span>1.431 kb</span>
      </div>
      <div class="actions">
        
        <a href="/defunkt/emacs/raw/master/vendor/treetop.el" id="raw-url">raw</a>
        
          <a href="/defunkt/emacs/blame/master/vendor/treetop.el">blame</a>
        
        <a href="/defunkt/emacs/commits/master/vendor/treetop.el">history</a>
      </div>
    </div>
    
  <div class="data syntax">
    
      <table cellpadding="0" cellspacing="0">
        <tr>
          <td>
            
            <pre class="line_numbers">
<span id="LID1" rel="#L1">1</span>
<span id="LID2" rel="#L2">2</span>
<span id="LID3" rel="#L3">3</span>
<span id="LID4" rel="#L4">4</span>
<span id="LID5" rel="#L5">5</span>
<span id="LID6" rel="#L6">6</span>
<span id="LID7" rel="#L7">7</span>
<span id="LID8" rel="#L8">8</span>
<span id="LID9" rel="#L9">9</span>
<span id="LID10" rel="#L10">10</span>
<span id="LID11" rel="#L11">11</span>
<span id="LID12" rel="#L12">12</span>
<span id="LID13" rel="#L13">13</span>
<span id="LID14" rel="#L14">14</span>
<span id="LID15" rel="#L15">15</span>
<span id="LID16" rel="#L16">16</span>
<span id="LID17" rel="#L17">17</span>
<span id="LID18" rel="#L18">18</span>
<span id="LID19" rel="#L19">19</span>
<span id="LID20" rel="#L20">20</span>
<span id="LID21" rel="#L21">21</span>
<span id="LID22" rel="#L22">22</span>
<span id="LID23" rel="#L23">23</span>
<span id="LID24" rel="#L24">24</span>
<span id="LID25" rel="#L25">25</span>
<span id="LID26" rel="#L26">26</span>
<span id="LID27" rel="#L27">27</span>
<span id="LID28" rel="#L28">28</span>
<span id="LID29" rel="#L29">29</span>
<span id="LID30" rel="#L30">30</span>
<span id="LID31" rel="#L31">31</span>
<span id="LID32" rel="#L32">32</span>
<span id="LID33" rel="#L33">33</span>
<span id="LID34" rel="#L34">34</span>
<span id="LID35" rel="#L35">35</span>
<span id="LID36" rel="#L36">36</span>
<span id="LID37" rel="#L37">37</span>
<span id="LID38" rel="#L38">38</span>
<span id="LID39" rel="#L39">39</span>
<span id="LID40" rel="#L40">40</span>
</pre>
          </td>
          <td width="100%">
            
            
              <div class="highlight"><pre><div class="line" id="LC1"><span class="c1">; from http://github.com/hornbeck/public_emacs</span></div><div class="line" id="LC2">&nbsp;</div><div class="line" id="LC3"><span class="p">(</span><span class="nf">defvar</span> <span class="nv">treetop-mode-hook</span> <span class="nv">nil</span><span class="p">)</span></div><div class="line" id="LC4">&nbsp;</div><div class="line" id="LC5"><span class="p">(</span><span class="nf">defvar</span> <span class="nv">treetop-mode-map</span></div><div class="line" id="LC6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="p">(</span><span class="k">let </span><span class="p">((</span><span class="nf">treetop-mode-map</span> <span class="p">(</span><span class="nf">make-keymap</span><span class="p">)))</span></div><div class="line" id="LC7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="p">(</span><span class="nf">define-key</span> <span class="nv">treetop-mode-map</span> <span class="s">&quot;\C-j&quot;</span> <span class="ss">&#39;newline-and-indent</span><span class="p">)</span></div><div class="line" id="LC8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="nv">treetop-mode-map</span><span class="p">)</span></div><div class="line" id="LC9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="s">&quot;Keymap for treetop major mode&quot;</span><span class="p">)</span></div><div class="line" id="LC10">&nbsp;</div><div class="line" id="LC11"><span class="p">(</span><span class="nf">add-to-list</span> <span class="ss">&#39;auto-mode-alist</span> <span class="o">&#39;</span><span class="p">(</span><span class="s">&quot;\\.treetop\\&#39;&quot;</span> <span class="o">.</span> <span class="nv">treetop-mode</span><span class="p">))</span></div><div class="line" id="LC12">&nbsp;</div><div class="line" id="LC13"><span class="p">(</span><span class="nf">defconst</span> <span class="nv">treetop-font-lock-keywords</span></div><div class="line" id="LC14">&nbsp;&nbsp;<span class="p">(</span><span class="nf">list</span></div><div class="line" id="LC15">&nbsp;&nbsp;&nbsp;<span class="o">&#39;</span><span class="p">(</span><span class="s">&quot;\\&lt;\\grammar\\|rule\\|def\\|end\\&gt;&quot;</span> <span class="o">.</span> <span class="nv">font-lock-builtin-face</span><span class="p">))</span></div><div class="line" id="LC16">&nbsp;&nbsp;&nbsp;<span class="s">&quot;Minimal highlighting expressions for treetop mode&quot;</span><span class="p">)</span></div><div class="line" id="LC17">&nbsp;</div><div class="line" id="LC18"><span class="p">(</span><span class="nf">defvar</span> <span class="nv">treetop-mode-syntax-table</span></div><div class="line" id="LC19">&nbsp;&nbsp;<span class="p">(</span><span class="k">let </span><span class="p">((</span><span class="nf">treetop-mode-syntax-table</span> <span class="p">(</span><span class="nf">make-syntax-table</span><span class="p">)))</span></div><div class="line" id="LC20">&nbsp;&nbsp;&nbsp;&nbsp;<span class="p">(</span><span class="nf">modify-syntax-entry</span> <span class="nv">?_</span> <span class="s">&quot;w&quot;</span> <span class="nv">treetop-mode-syntax-table</span><span class="p">)</span></div><div class="line" id="LC21">&nbsp;&nbsp;&nbsp;&nbsp;<span class="p">(</span><span class="nf">modify-syntax-entry</span> <span class="nv">?/</span> <span class="s">&quot;. 124b&quot;</span> <span class="nv">treetop-mode-syntax-table</span><span class="p">)</span></div><div class="line" id="LC22">&nbsp;&nbsp;&nbsp;&nbsp;<span class="p">(</span><span class="nf">modify-syntax-entry</span> <span class="nv">?*</span> <span class="s">&quot;. 23&quot;</span> <span class="nv">treetop-mode-syntax-table</span><span class="p">)</span></div><div class="line" id="LC23">&nbsp;&nbsp;&nbsp;&nbsp;<span class="p">(</span><span class="nf">modify-syntax-entry</span> <span class="nv">?</span><span class="err">\</span><span class="nv">n</span> <span class="s">&quot;&gt; b&quot;</span> <span class="nv">treetop-mode-syntax-table</span><span class="p">)</span></div><div class="line" id="LC24">&nbsp;&nbsp;&nbsp;&nbsp;<span class="nv">treetop-mode-syntax-table</span><span class="p">)</span></div><div class="line" id="LC25">&nbsp;&nbsp;<span class="s">&quot;Syntax table for treetop-mode&quot;</span><span class="p">)</span></div><div class="line" id="LC26">&nbsp;</div><div class="line" id="LC27"><span class="p">(</span><span class="nf">defun</span> <span class="nv">treetop-mode</span> <span class="p">()</span></div><div class="line" id="LC28">&nbsp;&nbsp;<span class="s">&quot;Major mode for editing treetop files&quot;</span></div><div class="line" id="LC29">&nbsp;&nbsp;<span class="p">(</span><span class="nf">interactive</span><span class="p">)</span></div><div class="line" id="LC30">&nbsp;&nbsp;<span class="p">(</span><span class="nf">kill-all-local-variables</span><span class="p">)</span></div><div class="line" id="LC31">&nbsp;&nbsp;<span class="p">(</span><span class="nf">set-syntax-table</span> <span class="nv">treetop-mode-syntax-table</span><span class="p">)</span></div><div class="line" id="LC32">&nbsp;&nbsp;<span class="p">(</span><span class="nf">use-local-map</span> <span class="nv">treetop-mode-map</span><span class="p">)</span></div><div class="line" id="LC33">&nbsp;&nbsp;<span class="p">(</span><span class="nf">set</span> <span class="p">(</span><span class="nf">make-local-variable</span> <span class="ss">&#39;font-lock-defaults</span><span class="p">)</span> <span class="o">&#39;</span><span class="p">(</span><span class="nv">treetop-font-lock-keywords</span><span class="p">))</span></div><div class="line" id="LC34">&nbsp;&nbsp;<span class="p">(</span><span class="nf">set</span> <span class="p">(</span><span class="nf">make-local-variable</span> <span class="ss">&#39;indent-line-function</span><span class="p">)</span> <span class="ss">&#39;treetop-indent-line</span><span class="p">)</span></div><div class="line" id="LC35">&nbsp;&nbsp;<span class="p">(</span><span class="nf">local-set-key</span> <span class="s">&quot;\t&quot;</span> <span class="s">&quot;  &quot;</span><span class="p">)</span></div><div class="line" id="LC36">&nbsp;&nbsp;<span class="p">(</span><span class="nf">setq</span> <span class="nv">major-mode</span> <span class="ss">&#39;treetop-mode</span><span class="p">)</span></div><div class="line" id="LC37">&nbsp;&nbsp;<span class="p">(</span><span class="nf">setq</span> <span class="nv">mode-name</span> <span class="s">&quot;treetop&quot;</span><span class="p">)</span></div><div class="line" id="LC38">&nbsp;&nbsp;<span class="p">(</span><span class="nf">run-hooks</span> <span class="ss">&#39;treetop-mode-hook</span><span class="p">))</span></div><div class="line" id="LC39">&nbsp;</div><div class="line" id="LC40"><span class="p">(</span><span class="nf">provide</span> <span class="ss">&#39;treetop</span><span class="p">)</span></div></pre></div>
            
          </td>
        </tr>
      </table>
    
  </div>


  </div>
</div>

    
  


  </div>

      
      
      <div class="push"></div>
    </div>
    
    <div id="footer">
      <div class="site">
        <div class="info">
          <div class="links">
            <a href="http://github.com/blog/148-github-shirts-now-available">T-Shirts</a> |            
            <a href="/blog">Blog</a> |             
            <a href="http://support.github.com/">Support</a> |
            <a href="/training">Git Training</a> | 
            <a href="/contact">Contact</a> |
            <a href="http://groups.google.com/group/github/">Google Group</a> | 
            <a href="http://github.wordpress.com">Status</a> 
          </div>
          <div class="company">
            <span id="_rrt" title="0.09798s from xc88-s00008">GitHub</span>
            is <a href="http://logicalawesome.com/">Logical Awesome</a> | &copy;2008 | <a href="/site/terms">Terms of Service</a> | <a href="/site/privacy">Privacy Policy</a>
          </div>
        </div>
        <div class="sponsor">
          <a href="http://engineyard.com"><img src="/images/modules/footer/engine_yard_logo.png" alt="Engine Yard" /></a>
          <div>
            Hosting provided by our<br /> partners at Engine Yard
          </div>
        </div>
      </div>
    </div>
    
    <div id="coming_soon" style="display:none;">
      This feature is coming soon.  Sit tight!
    </div>

    
        <script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
    var pageTracker = _gat._getTracker("UA-3769691-2");
    pageTracker._initData();
    pageTracker._trackPageview();
    </script>

  </body>
</html>

