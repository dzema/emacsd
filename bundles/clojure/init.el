;;(require 'clojure-mode)
;;(require 'clojure-snippet-helpers)

;; load bundle snippets
;; (yas/load-directory "~/emacsd/bundles/clojure/snippets")

;;(require 'clojure-cheatsheet)

(add-to-list 'auto-mode-alist '("\\.clj$" . clojure-mode))

;; CIDER

(setq cider-repl-pop-to-buffer-on-connect nil)
(setq cider-repl-use-clojure-font-lock t)

(add-hook 'clojure-mode-hook 'cider-mode)
(add-hook 'cider-mode-hook 'cider-turn-on-eldoc-mode)
(add-hook 'cider-repl-mode-hook 'paredit-mode)

;; Paredit

(global-set-key (kbd "M-?") 'paredit-convolute-sexp)
(add-hook 'clojure-mode-hook 'paredit-mode)

;; Rainbow delimiters

(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
