;;(yas/load-directory "~/emacsd/bundles/javascript/snippets")

;;(require 'js2)
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.json\\'" . js2-mode))

(add-to-list 'interpreter-mode-alist '("node" . js2-mode))
