Installation
============

Clone this repo to ~/emacsd dir.

Install the following modules from MELPA or other repository

    (ace-jump-mode browse-kill-ring change-inner cider clojure-mode cyberpunk-theme dash-at-point expand-region flx-ido flx flycheck f helm-ack helm-dash helm js2-mode json-mode jsx-mode magit git-rebase-mode git-commit-mode markdown-mode memory-usage paredit paredit-menu php-mode pkg-info epl dash rainbow-delimiters rspec-mode ruby-tools s scala-mode2 smex unicode-fonts ucs-utils font-utils persistent-soft list-utils pcache web-mode yasnippet)


You can try to use the following code to install packages:

```

; list the packages you want
(setq package-list '(ace-jump-mode browse-kill-ring cider clojure-mode cyberpunk-theme expand-region flx-ido flx flycheck js2-mode json-mode jsx-mode magit  markdown-mode memory-usage paredit php-mode pkg-info epl dash rainbow-delimiters rspec-mode ruby-tools s smex unicode-fonts ucs-utils font-utils persistent-soft list-utils pcache web-mode rinari))

; list the repositories containing them
(setq package-archives '(("melpa-stable" . "http://stable.melpa.org/packages/")
             (" elpa" . "http://tromey.com/elpa/")
                         ("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")))

                    ; activate all the packages (in particular autoloads)

(setq gnutls-trustfiles (list "/usr/local/etc/libressl/cert.pem"))
(setq gnutls-verify-error t)
(package-initialize)

; fetch the list of packages available 
(unless package-archive-contents
  (package-refresh-contents))

; install the missing packages
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))


```
    (defun ensure-package-installed (&rest packages)
      "Assure every package is installed, ask for installation if it’s not.
    Return a list of installed packages or nil for every package not installed."
      (mapcar
       (lambda (package)
         (package-installed-p 'evil)
         (if (package-installed-p package)
             package
           (if (y-or-n-p (format "Package %s is missing. Install it? " package))
               (package-install package)
             nil)))
       packages))

Add the following code to you ~/.emacs

    ;; set up unicode
    (prefer-coding-system       'utf-8)
    (set-default-coding-systems 'utf-8)
    (set-terminal-coding-system 'utf-8)
    (set-keyboard-coding-system 'utf-8)
    ;; This from a japanese coder.  I hope it works.
    (setq default-buffer-file-coding-system 'utf-8)
    ;; From Emacs wiki
    (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

    (defconst POSTGRESQL-BIN "/usr/local/bin")
    (defconst PATH "/System/Library/Frameworks/JavaVM.framework/Versions/1.6/Commands:/Users/dzema/bin:/Users/dzema/.cabal/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin")

    (setq exec-path (cons POSTGRESQL-BIN exec-path))
    (setenv "PATH" PATH)

    (defvar emacsd-dir     "~/emacsd/")

    (add-to-list 'load-path "~/.emacs.d")
    (add-to-list 'load-path emacsd-dir)

    ;; These MUST be loaded first
    ;;(defvar byte-compile-warnings f)
    ;;(defvar byte-compile-verbose f)
    ;;(load "byte-code-cache")

    (load "boot")

    (put 'set-goal-column 'disabled nil)

    ;; open files droped to emacs icon or provided via
    ;; open -a Emacs.app ./file.rb into existing frame
    ;; default is to create new window for each dropped
    ;; file
    (setq ns-pop-up-frames nil)
