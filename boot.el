
(setq custom-file (concat emacsd-dir "custom-variables.el"))

;;
;; Initializers
;;

(defun load-initializer (name)
  "Loads initializer with name NAME"
  (load (concat "initializers/" name "_initializer"))
  (message (concat "Loaded " name)))

;; These MUST be loaded first if you want to use
;; byte-code-cache.
;;
;; However, idea of precompilation seem to
;; work better.
;;

;;(defvar byte-compile-warnings f)
;;(defvar byte-compile-verbose f)
;;(load-initializer "bytecode_cache")


(load-initializer "tuning")
(load-initializer "package")
(load-initializer "path")
(load-initializer "keymap")
(load-initializer "kdb_macros")
(load-initializer "yasnippet")
(load-initializer "modes")
(load-initializer "ido")
(load-initializer "bundles")
(load-initializer "fonts")
(load-initializer "ui")
(load-initializer "server")
(load-initializer "spelling")

(load "custom-variables")
(load "custom-keymap")

(color-theme-solarized-dark)
;;(color-theme-textmate-twilight)
